#!perl

#fishlabsoman may 2015
#SkypePerlBot2000

use warnings;
use strict;

use AnyEvent;
use AnyEvent::DBus;
use Net::DBus::Skype::API;

#other
use threads;
use threads::shared;
use Time::HiRes;
#use IO::File;

use Encode;

use Data::Dumper qw(Dumper);

$| = 1;

my @lastIDs : shared;
@lastIDs = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15); #15 items history


#не решил что с ними делать.
my @notifys : shared;
my $ask : shared;
my $asks : shared;
my $send : shared;
my $sendingStatus : shared;
#end не решил



my $cv = AE::cv;#create event

my $login :shared;
my $nick :shared;
$login = "";
$nick = "";

$ask = 0;
$sendingStatus = 0;

my %logins;
my $delay = 3;



#use utf8;
#binmode STDOUT, ':utf8'; #need fix the message 'Wide character in print at...'
#binmode STDERR, ':utf8';
#binmode STDIN , ':utf8';


sub getWeather{#return weather on city

	my $city = shift;
	#print "$city'\n";
	#$city = decode_utf8($city);# O_O #q: decode all skype messages & use utf8 pragma?

	if($city =~ /^[\sA-яёЁa-z0-9_-]{3,40}$/u){#filter bad words
		#print "OK: Город: $city\n";
		$city =~ s/ /-/g;
	}else{
		#print "Error: Город не прошел проверку: $city\n";
		$city = "saint-petersburg";
	}
	
	my $content = `wget "http://pogoda.yandex.ru/$city/" -q -O -`;
	my $temp = ""; 
	unless($content){#if 404 or smth else
		$temp = "Город не найден.";
		return $temp;#искать или не искать дальше, вот в чем вопрос...
		$content = `wget http://pogoda.yandex.ru/saint-petersburg/ -q -O -`;#default
		unless($content){#if 404 or smth else AGAIN
			return "Невозможно получить погоду. Ответ пуст.";
		}
	}

	$content =~ /<h1 class="title title_level_1">(.+?)<\/h1>/;#title
	$temp.= "$1: ";

	if($content =~/(\+|-)(\w+)&thinsp;°C/u){
		$temp.= "$1$2°C";
	}else{
		return "Невозможно получить погоду. Не найдено на странице.";
	}

	if($content =~ /<span class="current-weather__comment">(.+?)<\/span>/){
		$temp .= "; $1";
	}

	if($content =~ /утром(.+?)current-weather__thermometer_type_after">(.+?)<\/div>/){#morning
		$temp .= "; утром: $2";
	}

	if($content =~ /днем(.+?)current-weather__thermometer_type_after">(.+?)<\/div>/){#day
		$temp .= "; днём: $2";
	}

	if($content =~ /вечером(.+?)current-weather__thermometer_type_after">(.+?)<\/div>/){#evening
		$temp .= "; вечер: $2";
	}

	if($content =~ /ночью(.+?)current-weather__thermometer_type_after">(.+?)<\/div>/){#night
		$temp .= "; ночью: $2";
	}

	$content =~ /<span class="current-weather__info-label">Ветер: <\/span> (.+?) м\/с (.+?) title="Ветер: (.+?)">(.+?)<\/abbr>/;
	$temp .= "; ветер: $3 $1 м/с.";
	return $temp;
}


sub getBash{
	#TODO: (?s) to ex?? rly?
	my $content = `wget http://bash.org.ru/forweb/?u -q -O -`;
	$content =~ s/' \+ '//g;#replace obfuscate
	$content =~ s/&lt;/</g;#fix html
	$content =~ s/&gt;/>/g;
	$content =~ s/&quot;/'/g;

	$content =~ /<a href=\"http:\/\/bash.im\/quote\/(\d+)\">#/g;#get id
	my $quoteNum = $1;
	$content =~ /<div id="b_q_t" style="padding: 1em 0;">(.+?)<\/div>/g;#get quote
	$content = $1;
	$content=~s/<br\s*[\/]?>/\n/g;#replace br to \n
	return "http://bash.im/quote/$quoteNum\n$content\n";
}


sub worker{#send/receive skype api messages

	$cv = AE::cv; #DBUS + AE = POWER and non blocking

	my $skype = Net::DBus::Skype::API->new(
			name => "SkypePerlBot by fishlabsoman v0.1",
			notify => sub {
				push @notifys, @_;
			},
			protocol => 8,
	);
	$skype->attach;

	my $timer_repeated = AE::timer 0, 0.001, sub {#need for pereodical check (unlock while)
		$cv->send;
	};

	while(1){
		$cv->recv;#lock while. standup if ->send
		if($sendingStatus eq 1){
			#my $send = shift @sends;
			$ask = $skype->send_command($send);
			$sendingStatus = 0;
		}
		$cv = AE::cv;#lock back
	}
}#end of worker

sub getAsk {#copypaste var to shared && xz/ indian xui
	while($sendingStatus eq 1){};#wait
	$send = shift;#update global var
	$sendingStatus = 1;#update status
	while($sendingStatus eq 1){};#wait complete
	return $ask;#return ask
}


sub sendMessage{#send message on chat
	my $chatID = shift;
	my $message = shift;
	return getAsk "CHATMESSAGE $chatID /me $message";
}

sub getMembers{#return members on chat
	my $chatID = shift;
	my $tmp = getAsk "GET CHAT $chatID MEMBERS";
	print "----\n$tmp\n";

	if($tmp =~ /CHAT (.+?) MEMBERS (.*)/){
		return $2;
	}
}



my $thread = threads->new( \&worker );#create dbus reader/writer thread
#my $ruletka = Ruletka->new(\&sendMessage, \&getMembers);


sub processMsg{#process messages

	my %msg = %{shift()};

	foreach my $arg (@lastIDs){#check for duplicate
		#print "list: $arg\n";
		if($arg eq $msg{'msgID'}){
			#print "Уже обрабатывал\n";
			return;
		}
	}

	shift @lastIDs;#shift first
	push @lastIDs, $msg{'msgID'};#add new in the end

	if($msg{'status'} eq "READ"){#READ not process this shitt!
		return;
	}

	my @array = split /[,\s]\s*/, $msg{'body'}; #split by spaces

	#print "\n$array[0]\n";#handled cmd





	unless($logins{$msg{"fromLogin"}} || $msg{"fromLogin"} eq $login ){#first run cmd #disable limit for me
		$logins{$msg{"fromLogin"}} = time;
		print "first run\n";
	}elsif($logins{$msg{"fromLogin"}}+$delay > time){
		print "limit\n";
		return;
	}else{
		print "go!";	
	}
	$logins{$msg{"fromLogin"}} = time;

	#GET CHAT #nigrimmist/$938f8a154c506234 TOPIC
	#ALTER CHAT #nigrimmist/$938f8a154c506234 SETTOPIC TRUE System Administration

	if($array[0] =~ /^!(ping|пинг)/g){
		my $tmp = sendMessage($msg{'chatID'}, "pong, $msg{'fromName'}");
	}

	if($array[0] =~ /^!(members|count|who|кто)/g){
		my $tmp = sendMessage($msg{'chatID'}, getMembers($msg{'chatID'}));
	}

	if($array[0] =~ /^!(exec)/g){
		if($msg{'fromLogin'} eq $login){
			shift @array;
			my $tmp = sendMessage($msg{'chatID'}, getAsk("@array"));
		}else{
			my $tmp = sendMessage($msg{'chatID'}, "У вас недостаточно привилегий! (bandit)");
		}
	}

	if($array[0] =~ /^!(sad)/g){
		my $tmp = sendMessage($msg{'chatID'}, "Иисус любит тебя, $msg{'fromName'} (sun)");
	}

	if($array[0] =~ /^!(resolve)/g){
		my $thread = threads->new(sub {
			my $quote;
			unless($array[1]){
				$quote = "Usage: !resolve <login>";
			}elsif($array[1] =~ /^[a-z0-9._-]{3,20}$/u){
				$quote = `php plugins/getResolve.php $array[1]`;
			}else{
				$quote = "Invalid login";
			}
			my $tmp = sendMessage $msg{'chatID'}, $quote ;
		});
	}

	if($array[0] =~ /^!(bash|баш|бащ)/g){
		my $quote = getBash();
		my $tmp = sendMessage $msg{'chatID'}, $quote ;
	}

	if($array[0] =~ /^!(cat|кот|котики|cats|сфе|rjn)/g){
		my $quote = getBash();
		my $tmp = sendMessage $msg{'chatID'}, `wget http://random.cat/meow -q -O -` ;
	}

	if($array[0] =~ /^!(ith|ithappens|итх)/g){
		my $quote = `php plugins/getIThappens.php`;
		my $tmp = sendMessage $msg{'chatID'}, $quote ;
	}

	if($array[0] =~ /^!(сиськи|boobs|сиси|tits|boobs)/g){
		my $quote = `php plugins/getTits.php`;
		my $tmp = sendMessage $msg{'chatID'}, $quote ;
	}

	if($array[0] =~ /^!(попки|попка|butts|butt)/g){
		my $quote = `php plugins/getButts.php`;
		my $tmp = sendMessage $msg{'chatID'}, $quote ;
	}

	if($array[0] =~ /^!(хабр|habr|хабрахабр|habrahabr)/g){
		my $quote = `php plugins/getHabr.php`;
		my $tmp = sendMessage $msg{'chatID'}, $quote ;
	}

	if($array[0] =~ /^!(комикс|comics|комиксы)/g){
		my $quote = `php plugins/getComics.php`;
		my $tmp = sendMessage $msg{'chatID'}, $quote ;
	}

	if($array[0] =~ /^!(погода|weather)/g){

		shift @array;
		my $tmp = sendMessage $msg{'chatID'}, getWeather("@array") ;
		return;
	}

	if($array[0] =~ /^!(uname)/g){
		my $tmp = sendMessage $msg{'chatID'}, `uname -a`;
	}

	if($array[0] =~ /^!(время|time)/g){
		my $tmp = sendMessage $msg{'chatID'}, `date`;
	}

	if($array[0] =~ /^!(help|помощь|команды|commands|about|хелп)/g){
		my $help	=	"Доступные команды: ".
						"!ping - возвращает pong\n".
						"!погода !weather <город>\n".
						"!комикс !comics !комиксы\n".
						"!хабр !habr !хабрахабр !habrahabr\n".
						"!попки !попка !butts !butt\n".
						"!сиськи !boobs !сиси !tits\n".
						"!ith !ithappens !итх\n".
						"!bash !баш !бащ\n".
						"!cat !cats !кот !коты !котики !кошки\n".
						"!uname\n".
						"!who !кто - кто в чате\n".
						"!exec (owner only)\n".
						"!resolve <login> - получение ип юзера\n".
						"!help !помощь !хелп !commands !команды !about\n".
						"SkypePerlBot, автор FisHlaBsoMAN. Репо: https://goo.gl/h86hf6 \n";
		my $tmp = sendMessage $msg{'chatID'}, $help ;
	}


	if($array[0] =~ /^!(rreg|ккуп|hhtu|ррег|ruletka|рулетка|суицид|покончитьссобой|убитьсебя|рхелп|рпомощь|h\[tkh|крудз|rclear|рклиар|radd|rgo|рго)/g){
		#print "рулетка \n";

		#my $thread = threads->new(sub {
			#my $tmp = $ruletka->run($msg{'chatID'}, $msg{'body'}, $msg{'fromLogin'}, "вася");
		#});
	}
}#end processMsg

sub checkChatNames{
	while(1){
		open(InFile, "conf/chat_names.txt") || return "Невозможно открыть файл conf/chat_names.txt на чтение\n";
		while (my $line = <InFile>){
			if($line =~ /(.+?) (.*)/){
				my $chatID = $1;
				my $name = $2;
				my $tmp = getAsk("GET CHAT $chatID TOPICXML");

				if($tmp =~ /CHAT (.+?) TOPICXML (.*)/){

					if($1 eq $chatID){
						#print STDERR "ok: chatID is compare\n";

						if($2 eq $name){
							#print STDERR "ok name \"$name\" vs \"$2\"\n";
						}else{
							print STDERR "NOT ok name \"$name\" vs \"$2\"\n";
							my $tmp = getAsk("ALTER CHAT $chatID SETTOPICXML $name");
							print STDERR "Response: $tmp\n";
						}
					}else{#else eq chatID

						print STDERR "Chat id not compare in response \"$chatID\" - \"$1\"\n"
					}#end if eq chatID

				}else{

					print STDERR "Bad response: \"$tmp\"!\n";
				}#end if parse resp regex

			}else{#else $line regex
				print STDERR "Bad line in file chat_names.txt: \"$line\"\n";
			}#end if $line regex
		}#end filereader while
		close ( InFile );
		sleep(5);
	}#end main while
}#end checkChatNames sub


my $thread2 = threads->new( \&checkChatNames );#create "watchdog" thread


while(1){#main thread

	Time::HiRes::usleep(50);#delay

	if(scalar @notifys > 0){#count of @notifies

		my $notification = shift @notifys;

		if($notification =~ /CURRENTUSERHANDLE (\w+)/ ) {#your login
			$login = $1;
			my $tmp = getAsk "GET USER $1 FULLNAME";#your nick,name etc
			$tmp =~ /USER (\w+) FULLNAME (.*)/g;
			$nick = $2;
			print "\033[032m[NameManager] \033[0m\033[2mYour name: \033[031m$2.\033[0m\n";

		}elsif ($notification =~ /USER (\w+) ONLINESTATUS (OFFLINE|ONLINE|AWAY|DND|INVISIBLE)/ ) {#online status other users

			my $status = $2;
			my $tmp = getAsk "GET USER $1 FULLNAME";
			$tmp =~ /USER (\w+) FULLNAME (.*)/g;
			print "\033[034m[OnlineStatus] \033[0m\033[2m User $1 ($2) has gone \033[031m$status.\033[0m\n";


		}elsif($notification =~ /CONNSTATUS ONLINE|CONNECTING|OFFLINE/g ) {# your connection status

			#print "\033[034m[ConnectionStatus] \033[0m\033[2m you has gone \033[031m$1.\033[0m\n";

		}elsif($notification =~ /USERSTATUS (OFFLINE|ONLINE|AWAY|DND|INVISIBLE)/ ) {# your online status

			print "\033[032m[YourStatus] \033[0m\033[2myou has gone \033[031m$1.\033[0m\n";


		}elsif($notification =~ /PROFILE MOOD_TEXT (.*)/ ) {# your status/profile text

			print "\033[032m[MoodText] \033[0m\033[2mhas been changed to \033[031m$1.\033[0m\n";

		}elsif($notification =~ /USER (\w+) MOOD_TEXT (.*)/ ) {# other users status/profile text

			print "\033[034m[MoodText] \033[0m\033[2muser $1 change text to \033[031m$2.\033[0m\n";

		}elsif($notification =~ /CHATMESSAGE (\d+) STATUS (\w+)/ ) {#read message

			my %msg;#new hash

			$msg{'msgID'} = $1;
			$msg{'status'} = $2;

			my $tmp = getAsk "GET CHATMESSAGE $1 CHATNAME";#receivе chatname - chat id
			$tmp =~ /CHATMESSAGE (\d+) CHATNAME (.*)/;
			$msg{'chatID'} = $2;

			$tmp = getAsk "GET CHATMESSAGE $msg{'msgID'} BODY";#receive message body
			$tmp =~ /(?s)CHATMESSAGE (\d+) BODY (.*)/;
			$msg{'body'} = $2;

			$tmp = getAsk "GET CHATMESSAGE $msg{'msgID'} TIMESTAMP";#receive time
			$tmp =~ /CHATMESSAGE (\d+) TIMESTAMP (\d+)/;
			$msg{'timestamp'} = $2;

			$tmp = getAsk "GET CHATMESSAGE $msg{'msgID'} FROM_HANDLE";#receive login
			$tmp =~ /CHATMESSAGE (\d+) FROM_HANDLE (.*)/;
			$msg{'fromLogin'} = $2;

			$tmp = getAsk "GET CHATMESSAGE $msg{'msgID'} FROM_DISPNAME";#receive name, nick
			$tmp =~ /CHATMESSAGE (\d+) FROM_DISPNAME (.*)/;
			$msg{'fromName'} = $2;

			my $date = `date -d \@$msg{'timestamp'}`;#readable date

			$date =~ s/^\s+|\s+$//g;#trim()
			$date =~ s/\h+/ /g;

			print "\033[035m[chat] \033[0m\033[0m $date, $msg{'status'}, MsgID:$msg{'msgID'}, ChatID:$msg{'chatID'}\033[0m\n";
			print "\033[032m ᒪ[chat] \033[0m\033[036m$msg{'fromName'} ($msg{'fromLogin'})\033[0m: \033[1m$msg{'body'}\033[0m\n\033[24m";

			#$tmp = getAsk "OPEN CHAT #$msg{'fromLogin'}/$msg{'chatID'}";#не воркает

			if($msg{"body"} =~ /^!/g){##### COMMANDS
				processMsg \%msg;
			}

		}elsif($notification =~ /CHAT (.*.) ACTIVITY_TIMESTAMP (\w+)/ ) {#activity timestamps

			my $chat = $1;
			my $date = `date -d \@$2`;#readable date
			$date =~ s/^\s+|\s+$//g;#trim()
			$date =~ s/\h+/ /g;

			#print "\033[035m[chatActivity] \033[0m\033[2m $date $chat \033[0m\n";#skip

		#}elsif($notification =~ /CHATMESSAGE (\d+) (EDITED_TIMESTAMP|EDITED_BY|BODY) (.*)/ ) {#skip edited messages
			#nothing only skipping
			#TODO: запилить логгинг :D
		}else{
			print "\033[033m[other] \033[0m\033[2m".$notification."\033[0m\n";
		}
	}#end of count notify > 1
}#end main while

$cv->recv;#wait













